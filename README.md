## 使用フレームワーク&ライブラリ
- Cordova
- Vue.js
- Materialize (こだわりはないのでなんでも良い)

## Cordovaの環境構築
（https://qiita.com/tagosaku324/items/ae0fb7326047e54a8497）  
（http://techblog.raccoon.ne.jp/archives/50600588.html）  
この辺りのリンクを参考にして実行環境を作る

## 実行
・ブラウザで実行する場合

```bash
> cordova run browser
```
・iPhone（実機）で実行する場合

```bash
> cordova run ios --device
```

その他の実行方法は上記リンク参照