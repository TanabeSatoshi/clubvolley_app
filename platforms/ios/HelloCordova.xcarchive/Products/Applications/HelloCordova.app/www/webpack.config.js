module.exports = {
    entry: {
	index: './js/index.js'
    },
    output: {
	path: __dirname + '/www/js',
	filename: '[name].bundle.js'
    }
};
