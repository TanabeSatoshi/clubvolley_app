(function ($) {
    $(function () {
	$('.modal').modal();
	$('#modal1').modal('open');
    });
})(jQuery);


var sort = new Vue ({
    el: '#sort',
    data: {
	message: '',
	items:[
	    {no:1, name:'A さん', img:'img/ikaku.png', categoryNo:'1'},
	    {no:2, name:'B さん', img:'img/myo-n.png', categoryNo:'2'},
	    {no:3, name:'C さん', img:'img/shirokuro.jpg', categoryNo:'3'},
	    {no:4, name:'D さん', img:'img/goron.png', categoryNo:'4'},
	    {no:5, name:'E さん', img:'img/ikaku.png', categoryNo:'1'},
	    {no:6, name:'F さん', img:'img/myo-n.png', categoryNo:'2'},
	    {no:7, name:'G さん', img:'img/goron.png', categoryNo:'3'},
	    {no:8, name:'H さん', img:'img/shirokuro.jpg', categoryNo:'4'},
	    {no:9, name:'dummy', img:'img/shirokuro.jpg', categoryNo:'1'}
	]
    },
    methods: {
	deleteItem: function(item, index){
	    this.items.splice(index, 1);
	},
	
	deleteDummy: function(){
	    this.items.pop();
	},

	test: function(){
	    this.message = this.items;
	}
    }
});
